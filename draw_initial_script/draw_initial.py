#!/usr/bin/env python

# Python 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

## END_SUB_TUTORIAL


def all_close(goal, actual, tolerance):
    """
    Convenience method for testing if the values in two lists are within a tolerance of each other.
    For Pose and PoseStamped inputs, the angle between the two quaternions is compared (the angle
    between the identical orientations q and -q is calculated correctly).
    @param: goal       A list of floats, a Pose or a PoseStamped
    @param: actual     A list of floats, a Pose or a PoseStamped
    @param: tolerance  A float
    @returns: bool
    """
    if type(goal) is list:
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    elif type(goal) is geometry_msgs.msg.Pose:
        x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)
        # Euclidean distance
        d = dist((x1, y1, z1), (x0, y0, z0))
        # phi = angle between orientations
        cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

    return True


class MoveGroupUR5PythonInterface(object):
    """MoveGroupUR5PythonInterface"""

    def __init__(self):
        super(MoveGroupUR5PythonInterface, self).__init__()

        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("ur5_python_interface", anonymous=True)

        robot = moveit_commander.RobotCommander()

        scene = moveit_commander.PlanningSceneInterface()

        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
        ## trajectories in Rviz:
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )


        # Misc variables
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher

    def go_to_joint_state(self):

        move_group = self.move_group

        ##Joint Goals:
        ##Joint 0 - Shoulder pan
        ##Joint 1 - Shoulder lift
        ##Joint 2 - Elbow joint
        ##Joint 3 - Wrist joint 1
        ##Joint 4 - Wrist joint 2
        ##Joint 5 - Wrist joint 3
        ##By using basic geometry and trig calculations, and with the understanding
        ## that "tau" is a full revolution of the joint, I calculated a way to orient
        ## the end effector so that its axis is perpendicular to the ground and it
        ## is low to the ground to give it room to work in the z direction for
        ## drawing the initial
        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = 0
        joint_goal[1] = 0
        joint_goal[2] = 0
        joint_goal[3] = -tau / 4
        joint_goal[4] = -tau / 4
        joint_goal[5] = 0

        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    def plan_cartesian_path(self, scale=1):

        move_group = self.move_group

        waypoints = []

        wpose = move_group.get_current_pose().pose
        wpose.position.z += scale * 0.3  # First move up (z) for left edge of "B"
        waypoints.append(copy.deepcopy(wpose))

#Due to ignorance of using cartesian paths to map out curved trajectory,
#modified incremented positional movements to mimic "rounded" edges of 
#letter "B"
# 8 total movements, 4 paths taken to create a semicircular curve. this is
# repeated twice to create the two "rounded" regions of the letter "B"
        wpose.position.z -= scale * 0.025
        wpose.position.y += scale * 0.1
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.05
        wpose.position.y += scale * 0.1
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.05
        wpose.position.y -= scale * 0.1
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.025
        wpose.position.y -= scale * 0.1
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.025
        wpose.position.y += scale * 0.1
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.05
        wpose.position.y += scale * 0.1
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.05
        wpose.position.y -= scale * 0.1
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.025
        wpose.position.y -= scale * 0.1
        waypoints.append(copy.deepcopy(wpose))

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0,
        # ignoring the check for infeasible jumps in joint space, which is sufficient
        # for this basic movement.
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )

        return plan, fraction

    def display_trajectory(self, plan):
        robot = self.robot
        display_trajectory_publisher = self.display_trajectory_publisher

        display_trajectory = moveit_msgs.msg.DisplayTrajectory()
        display_trajectory.trajectory_start = robot.get_current_state()
        display_trajectory.trajectory.append(plan)

        display_trajectory_publisher.publish(display_trajectory)

    def execute_plan(self, plan):
        move_group = self.move_group
        move_group.execute(plan, wait=True)

def main():
    try:
        input(
            "============ Press `Enter` to begin setting up the moveit_commander ..."
        )
        tutorial = MoveGroupUR5PythonInterface()

        input(
            "============ Press `Enter` to execute a movement using a joint state goal ..."
        )
        tutorial.go_to_joint_state()

        input("============ Press `Enter` to plan and execute a Cartesian path ...")
        cartesian_plan, fraction = tutorial.plan_cartesian_path()

        tutorial.display_trajectory(cartesian_plan)

        tutorial.execute_plan(cartesian_plan)

        print("============ UR5e movements complete!")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()
