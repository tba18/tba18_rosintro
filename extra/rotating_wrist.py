#!/usr/bin/env python

# IMPORTS/PACKAGES USED IN TUTORIAL, 
# DID NOT WANT TO BREAK ANY PIECES OF THE CODE
# SO KEPT MOST JUST TO BE SAFE
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from gripperClass import *

import math
from time import sleep

try:
    from math import pi, tau, dist, fabs, cos
except:  
    # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    # tau is one full revolution for a joint, which is referenced later in
    #   the joint goals function
    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))

from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list



def all_close(goal, actual, tolerance):
    """
    Convenience method for testing if the values in two lists are within a tolerance of each other.
    For Pose and PoseStamped inputs, the angle between the two quaternions is compared (the angle
    between the identical orientations q and -q is calculated correctly).
    @param: goal       A list of floats, a Pose or a PoseStamped
    @param: actual     A list of floats, a Pose or a PoseStamped
    @param: tolerance  A float
    @returns: bool
    """
    if type(goal) is list:
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    elif type(goal) is geometry_msgs.msg.Pose:
        x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)
        # Euclidean distance
        d = dist((x1, y1, z1), (x0, y0, z0))
        # phi = angle between orientations
        cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

    return True
pass


'''===v===v===v===v===v===v===v===v===v===v===v===v===v===v===v===v===v===v===v===v===v==='''
class MoveGroupUR5PythonInterface(object):
    """
    MoveGroupUR5PythonInterface



    Attributes:
        box_name
        robot
        scene
        move_group
        display_trajectory_publisher    <-- ROS publisher to display trajectories in Rviz

        d1  <-- shoulder_height, type=float, unit=m
        a2  <-- upper_arm_length, type=float, unit=m
        a3  <-- forearm_length, type=float, unit=m
        d4  <-- wrist_1_length, type=float, unit=m
        d5  <-- wrist_2_length, type=float, unit=m
        d6  <-- wrist_3_length, type=float, unit=m

        platform_dist   <-- PLATFORM DISTANCE FROM BASE OF ROBOT, with clearance, type=float, unit=m
        cup_dist        <-- CUP DISTANCE FROM BASE OF ROBOT, with clearance, type=float, unit=m

        int_obstacle_lower  <-- LOWER BOUND, CLOSEST INTERIOR OBSTACLE RADIALLY WITHIN TASKSPACE,
                                accomodates radius of robot arm and metal pedestal, type=float, unit=m
        int_obstacle_perp   <-- PERP BOUND, type=float, unit=m
        int_obstacle_upper  <-- HIGHER BOUND, type=float, unit=m

        platform_loc    <-- indicates location of platform in space, type=float, unit=rad

        init_shoulder_angle <-- INIT SHOULDER ANGLE, type=float, unit=deg

        ee_to_gripper   <-- EE TO GRIPPER CENTER DISTANCES, type=float
        ee_to_origin    <-- EE TO origin DISTANCES, type=float

        initial Initial Variable    <-- indicates picking up or pouring stage, type=bool

        gripper_to_cup  <-- Gripper to cup distance on x-y plane, unit=m



    Methods:
        law_of_cosines()    <-- Solve triangle interior angles using three sides.

        compute_future_joint_state()    <-- Compute the joint state it need to be in to pick-up/pour.
                                            Returns joint values shoulder_joint, elbow_joint, wrist_1_joint.

        go_to_radial_joint_state()  <-- Point radially at the target and get ready to pick-up/pour

        init_rotational_position_turn() <-- Calculate joints 2/3/4 angles necessary for radial translation, 
                                            and move.

        init_rotational_position()  <-- Turn the gripper away from the wrist and stay clear from during the movement

        compute_shoulder_rotation() <-- Comupte the shoulder joint value it need to be at.
                                        Return future shoulder joint angle

        move_to_target()    <-- Calculate joint 1 angle necessary to move clockwise to target, 
                                and move.

        lift_bottle()

        find_shoulder_angle_change()    <-- Find the shoulder joint angle it need to change to pick-up/puor the bottle
        
        pour_bottle()

        perform_first_phase()   <-- Going from unknown state, move to init pos.
                                    Move to bottle with open gripper

        perform_second_phase()  <-- Once grip bottle, lift and return to init pos.

        perform_third_phase()   <-- After receive x,y coords of glass, move to target,
                                    sleep for pouring, return to init state
    """
    def __init__(self):
        '''
        Sets up our move_it commander and our particular robot used (URe5 "manipulator"),
        also setup the experiment environment parameters
        '''

        super(MoveGroupUR5PythonInterface, self).__init__()

        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("ur5_python_interface", anonymous=True)

        robot = moveit_commander.RobotCommander()

        scene = moveit_commander.PlanningSceneInterface()

        # group_name is the particular robotic arm used
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
        ## trajectories in Rviz:
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )


        # Misc variables
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher

        #LINK LENGTHS EXTRACTED FROM URDF FILE
        d1 = 0.08916 #shoulder_height
        a2 = 0.4250 # upper_arm_length
        a3 = 0.39225 # forearm_length
        d4 = 0.10915 # wrist_1_length
        d5 = 0.09465 # wrist_2_length
        d6 = 0.0823 # wrist_3_length
        self.d1 = d1
        self.a2 = a2
        self.a3 = a3
        self.d4 = d4
        self.d5 = d5
        self.d6 = d6

        # PLATFORM DISTANCE FROM BASE OF ROBOT
        true_platform_dist = 0.377825 # Based on measurements made in lab
        platform_dist = true_platform_dist - 0.12 # subtract 0.1 to consider
                                    # clearance necessary for radius
                                    # of arm, and also want to grab
                                    # bottle 0.1 meters off ground for COM
        self.platform_dist = platform_dist

        # CUP DISTANCE FROM BASE OF ROBOT
        cup_dist = platform_dist - 0.15 # subtract 0.3 to consider
                                    # clearance necessary for pouring
                                    # into cup below
        self.cup_dist = cup_dist

        # CLOSEST INTERIOR OBSTACLE RADIALLY WITHIN TASKSPACE
        # LOWER BOUND
        int_obstacle_lower = 0.2 # accomodates radius of robot arm and metal pedestal
        # PERP BOUND
        int_obstacle_perp = 0.315045 # accomodates radius of robot arm and metal pedestal
        # UPPER BOUND
        int_obstacle_upper = 0.437861 # accomodates radius of robot arm and metal pedestal
        self.int_obstacle_lower = int_obstacle_lower
        self.int_obstacle_perp = int_obstacle_perp
        self.int_obstacle_upper = int_obstacle_upper

        # PLATFORM LOC - indicates location of platform in space
        platform_loc = (5 * pi) / 4 #UR5e setup in lab
        # platform_loc = pi / 2 # x positive
        # platform_loc = -pi / 2 # x negative
        # platform_loc = pi # y positive
        # platform_loc = 0 # y negative
        self.platform_loc = platform_loc

        # INIT SHOULDER ANGLE
        init_shoulder_angle = 70# in degrees
        self.init_shoulder_angle = init_shoulder_angle

        # EE TO GRIPPER CENTER DISTANCES
        ee_to_gripper = 0.1349375
        ee_to_origin = 0.026824
        self.ee_to_gripper = ee_to_gripper
        self.ee_to_origin = ee_to_origin

        # Initial Variable - indicates picking up or pouring stage
        initial = True
        self.initial = initial

        # Gripper to Cup - distance on x-y plane
        gripper_to_cup = 0.02 # based own experiments pouring bottle into cup
        self.gripper_to_cup = gripper_to_cup
    pass

    def setup_camera(self):
        '''
        Turn the gripper away from the wrist and stay clear from during the movement
        '''
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = (3 * pi) / 4
        joint_goal[1] = (-tau * self.init_shoulder_angle) / 360
        joint_goal[2] = (tau * 85.96) / 360
        joint_goal[3] = (-tau * 17.522) / 360
        joint_goal[4] = pi / 2
        joint_goal[5] = -2 * pi # will be ultimate orientation 

        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)
        pass

    def rotate_camera(self):
        '''
        Turn the gripper away from the wrist and stay clear from during the movement
        '''
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()

        joint_goal[5] = 2 * pi # will be ultimate orientation 

        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)
        pass

    def rotate_camera_back(self):
        '''
        Turn the gripper away from the wrist and stay clear from during the movement
        '''
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()

        joint_goal[5] = - 2 * pi # will be ultimate orientation 

        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)
        pass


'''===v===v===v===v===v===v===v===v===v===v===v===v===v===v===v===v===v===v===v===v===v==='''
def main():
    try:
        # INIT
        move_it = MoveGroupUR5PythonInterface()
        # gripper = Gripper()
        # gripper.gripper_init()
        input("Ready to close gripper?")
        # gripper.CloseGripper()
        move_it.setup_camera()
        input("Rotate?")
        move_it.rotate_camera()
        input("Rotate again?")
        move_it.rotate_camera_back()

        print("Success")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return
pass


if __name__ == "__main__":
    main()
