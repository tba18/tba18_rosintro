#!/usr/bin/env python

# IMPORTS/PACKAGES USED IN TUTORIAL, DID NOT WANT TO BREAK ANY PIECES OF THE CODE
# SO KEPT MOST JUST TO BE SAFE
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    # tau is one full revolution for a joint, which is referenced later in
    #   the joint goals function
    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list


def all_close(goal, actual, tolerance):
    """
    Convenience method for testing if the values in two lists are within a tolerance of each other.
    For Pose and PoseStamped inputs, the angle between the two quaternions is compared (the angle
    between the identical orientations q and -q is calculated correctly).
    @param: goal       A list of floats, a Pose or a PoseStamped
    @param: actual     A list of floats, a Pose or a PoseStamped
    @param: tolerance  A float
    @returns: bool
    """
    if type(goal) is list:
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    elif type(goal) is geometry_msgs.msg.Pose:
        x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)
        # Euclidean distance
        d = dist((x1, y1, z1), (x0, y0, z0))
        # phi = angle between orientations
        cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

    return True


class MoveGroupUR5PythonInterface(object):
    """MoveGroupUR5PythonInterface"""

# INIT FUNCTION
#   Sets up our move_it commander and our particular robot used (URe5 "manipulator")
    def __init__(self):
        super(MoveGroupUR5PythonInterface, self).__init__()

        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("ur5_python_interface", anonymous=True)

        robot = moveit_commander.RobotCommander()

        scene = moveit_commander.PlanningSceneInterface()

        # group_name is the particular robotic arm used
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
        ## trajectories in Rviz:
        display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )


        # Misc variables
        self.box_name = ""
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.display_trajectory_publisher = display_trajectory_publisher

        print(robot.get_current_state())

# JOINT STATE INITIALIZER
#   orients the joints based on certain rotational angles, allows for manipulation of robotic arm
#   and orientation without implicit consideration for specific position of end effector in space
    def go_to_joint_state(self):

        move_group = self.move_group

        ##Joint Goals:
        ##Joint 0 - Shoulder pan
        ##Joint 1 - Shoulder lift
        ##Joint 2 - Elbow joint
        ##Joint 3 - Wrist joint 1
        ##Joint 4 - Wrist joint 2
        ##Joint 5 - Wrist joint 3
        ##By using basic geometry and trig calculations, and with the understanding
        ## that "tau" is a full revolution of the joint, I calculated a way to orient
        ## the end effector so that its axis is perpendicular to the ground and it
        ## is low to the ground to give it room to work in the positive z direction for
        ## drawing the initial
        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = 0
        joint_goal[1] = (-tau * 5) / 12
        joint_goal[2] = tau / 3
        joint_goal[3] = (-tau * 5) / 12
        joint_goal[4] = 0
        joint_goal[5] = tau / 6

        move_group.go(joint_goal, wait=True)

        move_group.stop()

        print(self.robot.get_current_state())
        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

# CARTESIAN PATH FOR DRAWING LETTER "T"
    def plan_cartesian_pathT(self, scale=1):

        move_group = self.move_group

        waypoints = []

        # Begin at base of letter "T"
        wpose = move_group.get_current_pose().pose
        wpose.position.z += scale * 0.15  # First move up (z) for center line of "T"
        waypoints.append(copy.deepcopy(wpose))

        #wpose.position.z += scale * 0.0005 
        wpose.position.x += scale * 0.04 # Then move left (-x direction) for beginning 
                                            # of upper portion of letter
        waypoints.append(copy.deepcopy(wpose))

        #wpose.position.z -= scale * 0.0005
        wpose.position.x -= scale * 0.08 # Finally, finish the top portion of "T" by moving
                                            # right (+x direction) across
        waypoints.append(copy.deepcopy(wpose))

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0,
        # ignoring the check for infeasible jumps in joint space, which is sufficient
        # for this basic movement.
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )

        return plan, fraction

# CARTESIAN PATH FOR DRAWING LETTER "B"
    def plan_cartesian_pathB(self, scale=1):

        move_group = self.move_group

        waypoints = []

        wpose = move_group.get_current_pose().pose
        wpose.position.z += scale * 0.2  # First move up (z) for left edge of "B"
        waypoints.append(copy.deepcopy(wpose))

#Due to ignorance of using cartesian paths to map out curved trajectory,
#modified incremented positional movements to mimic "rounded" edges of 
#letter "B"
# 8 total movements, 4 paths taken to create a semicircular curve. this is
# repeated twice to create the two "rounded" regions of the letter "B"
        wpose.position.z -= scale * 0.015
        wpose.position.x -= scale * 0.02 # first portion of upper curve
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.035
        wpose.position.x -= scale * 0.02 # finish quarter circle of upper curve
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.035
        wpose.position.x += scale * 0.02 # begin next portion of upper curve
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.015
        wpose.position.x += scale * 0.02 # finish upper portion by completing semicircle
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.015
        wpose.position.x -= scale * 0.02 # begin second semicircle/lower curve
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.035
        wpose.position.x -= scale * 0.02 # finish quarter circle of lower curve
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.035
        wpose.position.x += scale * 0.02 # begin next portion of lower curve
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.015
        wpose.position.x += scale * 0.02 # finish lower portion by completing semicircle
        waypoints.append(copy.deepcopy(wpose))

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0,
        # ignoring the check for infeasible jumps in joint space, which is sufficient
        # for this basic movement.
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )

        return plan, fraction

# CARTESIAN PATH FOR DRAWING LETTER "A"
    def plan_cartesian_pathA(self, scale=1):

        move_group = self.move_group

        waypoints = []

        wpose = move_group.get_current_pose().pose

        wpose.position.x += scale * 0.04 # Move end effector to bottom left corner of letter
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z += scale * 0.13
        wpose.position.x -= scale * 0.04 # Create first diagonal from bottom left to top of letter
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z -= scale * 0.13
        wpose.position.x -= scale * 0.04 # Create second diagonal from top of letter to bottom right
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.z += scale * 0.04875
        wpose.position.x += scale * 0.015 # Back-track letter trace to "middle" portion of right diagonal edge
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.x += scale * 0.06 # Complete trace of letter with right-to-left stripe through the diagonals
        waypoints.append(copy.deepcopy(wpose))

        # We want the Cartesian path to be interpolated at a resolution of 1 cm
        # which is why we will specify 0.01 as the eef_step in Cartesian
        # translation.  We will disable the jump threshold by setting it to 0.0,
        # ignoring the check for infeasible jumps in joint space, which is sufficient
        # for this basic movement.
        (plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0
        )

        return plan, fraction

# Display trajectory function used for visualization in Rviz
    def display_trajectory(self, plan):
        robot = self.robot
        display_trajectory_publisher = self.display_trajectory_publisher

        display_trajectory = moveit_msgs.msg.DisplayTrajectory()
        display_trajectory.trajectory_start = robot.get_current_state()
        display_trajectory.trajectory.append(plan)

        display_trajectory_publisher.publish(display_trajectory)

# Executes the movements appended in the plan_cartesian_path functions
    def execute_plan(self, plan):
        move_group = self.move_group
        move_group.execute(plan, wait=True)

        print(self.robot.get_current_state())

def main():
    try:
        input(
            "============ Press `Enter` to begin setting up the moveit_commander ..."
        )
        # INIT
        tutorial = MoveGroupUR5PythonInterface()

        input(
            "============ Press `Enter` to move to initial position to draw a 'T' ..."
        )
        # RETURN TO BASE JOINT STATE
        tutorial.go_to_joint_state()

        input("============ Press `Enter` to draw a 'T' ...")
        # DRAW LETTER "T"
        cartesian_plan, fraction = tutorial.plan_cartesian_pathT()

        tutorial.display_trajectory(cartesian_plan)

        tutorial.execute_plan(cartesian_plan)

        input(
            "============ Press `Enter` to move to initial position to draw a 'B' ..."
        )
        # RETURN TO BASE JOINT STATE
        tutorial.go_to_joint_state()

        input("============ Press `Enter` to draw a 'B' ...")
        # DRAW LETTER "B"
        cartesian_plan, fraction = tutorial.plan_cartesian_pathB()

        tutorial.display_trajectory(cartesian_plan)

        tutorial.execute_plan(cartesian_plan)

        input(
            "============ Press `Enter` to move to initial position to draw an 'A' ..."
        )
        # RETURN TO BASE JOINT STATE
        tutorial.go_to_joint_state()

        input("============ Press `Enter` to draw an 'A' ...")
        # DRAW LETTER "A"
        cartesian_plan, fraction = tutorial.plan_cartesian_pathA()

        tutorial.display_trajectory(cartesian_plan)

        tutorial.execute_plan(cartesian_plan)

        print("============ UR5e movements complete!")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()
