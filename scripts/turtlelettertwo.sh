#!/usr/bin/bash

rosservice call /spawn 2 3 0.3 "turtletwo"
rosservice call /turtle1/set_pen 200 15 150 4 off
rosservice call /turtletwo/set_pen 40 180 230 4 off
rostopic pub -1 /turtletwo/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtletwo/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 5.0, 0.0]' '[0.0, 0.0, -4.5]'
rostopic pub -1 /turtletwo/cmd_vel geometry_msgs/Twist \
	-- '[0.0, -5.0, 0.0]' '[0.0, 0.0, -4.5]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[8.0, 0.0, 0.0]' '[0.0, 0.0, 6.28]'
